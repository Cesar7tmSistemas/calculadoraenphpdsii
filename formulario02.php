<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar PHP dentro del mismo Formulario</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/style.css" >
    <link rel="stylesheet" href="css/cbs.css" >
    <link rel="stylesheet" href="css/estilo.css" >
</head>
<body>
    
    <form action="#" method="POST" >
    <legend>Calculadora en PHP</legend>
    <p>Numero1: <input type="text" name = "txtNro1"/></p>
    <p>Numero2: <input type="text" name = "txtNro2"/></p>
    <p>
    <input type="submit" name = "btnSumar" value = "Sumar"/>
    <input type="submit" name = "btnRestar" value = "Restar"/>
    <input type="submit" name = "btnMultiplicar" value = "Multiplicar"/>
    <input type="submit" name = "btnDividir" value = "Dividir"/>
    <input type="submit" name = "btnPotencia" value = "Potencia"/>
    <input type="submit" name = "btnFactorial" value = "Factorial"/>
    <input type="submit" name = "btnSeno" value = "Seno"/>
    <input type="submit" name = "btnCoseno" value = "Coseno"/>
    <input type="submit" name = "btnTangente" value = "Tangente"/>
    <input type="submit" name = "btnPorcentaje" value = "Porcentaje"/>
    <input type="submit" name = "btnRaizcuadrada" value = "Raizcuadrada"/>
    <input type="submit" name = "btnRaizEnesima" value = "RaizEnesima"/>
    <input type="submit" name = "btnInversa" value = "Inversa"/>

    </p>
    </form>
<?php
    //Llamar a la clase calculadora
    if($_POST)
    {
    include("calculadora.php");
    
    $calculo = new Calculadora;
    $nro1 = $_POST['txtNro1'];
    $nro2 = $_POST['txtNro2'];
   
    $calculo->nro1 = $nro1;
    $calculo->nro2 = $nro2;
    
    if(isset($_POST['btnSumar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La suma de los números es: ",$calculo->Sumar();
    }
    if(isset($_POST['btnRestar']))
    {   
        //Instanciar un objeto a traves de la clase
        echo "La resta de los numeros es: ",$calculo->Restar();
    }
    if(isset($_POST['btnMultiplicar']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La multiplicación de los numeros es: ",$calculo->Multiplicar();
    }
    if(isset($_POST['btnDividir']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La division de los numeros es: ",$calculo->Dividir();
    }
    if(isset($_POST['btnPotencia']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La potencia de los numeros es: ",$calculo->Potencia($nro1, $nro2);
    }
    if(isset($_POST['btnFactorial']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El facorial del numero es: ",$calculo->Factorial($nro1);
    }
    if(isset($_POST['btnSeno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El seno del numero es: ",$calculo->Seno($nro1);
    }
    if(isset($_POST['btnCoseno']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El coseno de numero es: ",$calculo->Coseno($nro1);
    }
    if(isset($_POST['btnTangente']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La tangente del numero es: ",$calculo->Tangente($nro1);
    }
    if(isset($_POST['btnPorcentaje']))
    {
        //Instanciar un objeto a traves de la clase
        echo "El porcentaje es: ",$calculo->Porcentaje($nro1);
    }
    if(isset($_POST['btnRaizcuadrada']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La raiz cuadrada del numero es: ",$calculo->RaizCuadrada($nro1);
    }
    if(isset($_POST['btnRaizEnesima']))
    {
        //Instanciar un objeto a traves de la clase
        echo "La raiz N esima de los números es: ",$calculo->RaizEnesima($nro1);
    }
    if(isset($_POST['btnInversa']))
    {
        //Instanciar un objeto a traves de la clase
        echo "EL inverso del numero es: ",$calculo->Inverso($nro1);
    }
    
}

?>
</body>
</html>